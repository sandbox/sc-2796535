<?php

/**
 * @file
 * Holds twitter tweets list template for cke_placeholder tab.
 */
?>

<div class="cke-placeholder-lists">
  <?php foreach ($tweets as $tweet): ?>
  <div draggable="true"
    class="cke-placeholder-draggable cke_placeholder_twitter_tweets"
    ondragstart="ckePlaceholder.dragStart(event, 'cke_placeholder_twitter_tweets')"
    data-id="<?php echo $tweet->id_str; ?>"
    data-text="<?php echo $tweet->text; ?>">

    <div class="content"><?php echo truncate_utf8($tweet->text, 90, TRUE, TRUE); ?></div>
    <?php if (!empty($tweet->user->name)): ?>
      <div class="author"><?php print $tweet->user->name; ?></div>
    <?php endif; ?>
  </div>
  <?php endforeach; ?>
</div>
