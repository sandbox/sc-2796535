<?php

/**
 * @file
 * Holds twitter integration admin interface.
 */

/**
 * Admin setting form.
 */
function twitter_integration_admin_settings_form($form, $form_state) {
  $form['twitter_integration_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer key'),
    '#default_value' => variable_get('twitter_integration_consumer_key', ''),
    '#required' => TRUE,
  );

  $form['twitter_integration_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer secret'),
    '#default_value' => variable_get('twitter_integration_consumer_secret', ''),
    '#required' => TRUE,
  );

  $form['twitter_integration_oauth_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Oauth access token'),
    '#default_value' => variable_get('twitter_integration_oauth_access_token', ''),
    '#required' => TRUE,
  );

  $form['twitter_integration_oauth_access_token_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Oauth access token secret'),
    '#default_value' => variable_get('twitter_integration_oauth_access_token_secret', ''),
    '#required' => TRUE,
  );

  $form['#prefix'] = t('<p>For addding required data, go to <a href="https://apps.twitter.com" target="_blank">https://apps.twitter.com</a> and create own Twitter application.</p>');

  return system_settings_form($form);
}
