This module provide a simple API for library twitter-api-php.
We use this module for fetching tweets from twitter and another modules can use it.

Example how to build via drush make:

```
libraries[twitter_api_php][type] = library
libraries[twitter_api_php][download][type] = git
libraries[twitter_api_php][download][url] = "https://github.com/J7mbo/twitter-api-php.git"
libraries[twitter_api_php][download][tag] = "1.0.5"
libraries[twitter_api_php][directory_name] = "twitter_api_php"
libraries[twitter_api_php][destination] = "libraries"
```

Go to admin/config/services/twitter_integration for adding needed settings.
For adding Consumer key, Consumer secret, Oauth access token and Oauth access token secret need to be created app on the https://apps.twitter.com.
