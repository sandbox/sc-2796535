This module extend "cke_placeholder" and add new plugin for dropping twitter tweets into text.
Also this module create a new tab "Twitter tweets" in "cke_placeholder" library and provide fetching tweets.

Before enabling module, download "cke_placeholder" from https://www.drupal.org/project/cke_placeholder.
